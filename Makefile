USER=pedal
CONT=pedal-guide
IMAGE=pedal-works-guide
SERVER_SSH=root@188.226.170.38
SERVER_FOLDER_SRC=/usr/src/pedal-works-guide

build-image:
	docker build --rm -t ${IMAGE} .

build-container:
	docker run \
		-v ${HOME}/.gitconfig:/${USER}/.gitconfig \
		-v ${HOME}/.ssh:/${USER}/.ssh \
		-v ${PWD}:/${USER}/src \
		-w /${USER}/src \
		-h guide \
		--dns=8.8.8.8 \
		-it \
		--name ${CONT} \
		${IMAGE}

attach-container:
	( ( docker stop ${CONT} ) || (echo "Container not found: ${CONT}"  && exit 0) ) && \
	docker start ${CONT} && \
	docker attach ${CONT}

build-deploy:
	npm install && \
	gulp build && \
	rsync -avzh --delete ${PWD}/dist/ ${PWD}/production/dist && \
	cd ${PWD}/production/ && npm install --production && cd ../ && \
	rsync -avzh --delete ${PWD}/production/ ${SERVER_SSH}:${SERVER_FOLDER_SRC} && \
	ssh ${SERVER_SSH} "cd ${SERVER_FOLDER_SRC} && make build-container"
