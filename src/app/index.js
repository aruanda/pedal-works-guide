(function() {
  'use strict';

  ////animação no icone de scroll
  function scrollIconAnimate() {
    $('#scrollIcon').addClass('animated bounce').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
      function() {
        $('#scrollIcon').removeClass('animated bounce');
        setTimeout(function() {
          scrollIconAnimate();
        }, 2000);
      });
  };

  if (!isMobile.any) {
    scrollIconAnimate();
  };

  //scroll animado
  $('.animateAnchor').on('click', function(event) {
    event.preventDefault();
    $('html, body').animate({
      scrollTop: $($(this).attr("href")).offset().top - 78
    }, 1000);
  });
})();

