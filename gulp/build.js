'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var allPlugins = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('partials', function () {
  return gulp.src([
    path.join(conf.paths.src, '/app/**/*.html'),
    path.join(conf.paths.tmp, '/serve/app/**/*.html')
  ])
    .pipe(allPlugins.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe(allPlugins.angularTemplatecache('templateCacheHtml.js', {
      module: 'pedal-workds-guide',
      root: 'app'
    }))
    .pipe(gulp.dest(conf.paths.tmp + '/partials/'));
});

gulp.task('html', ['inject', 'partials'], function () {
  var partialsInjectFile = gulp.src(path.join(conf.paths.tmp, '/partials/templateCacheHtml.js'), { read: false });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: path.join(conf.paths.tmp, '/partials'),
    addRootSlash: false
  };

  var htmlFilter = allPlugins.filter('*.html');
  var jsFilter = allPlugins.filter('**/*.js');
  var cssFilter = allPlugins.filter('**/*.css');
  var assets;

  return gulp.src(path.join(conf.paths.tmp, '/serve/*.html'))
    .pipe(allPlugins.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = allPlugins.useref.assets())
    .pipe(allPlugins.rev())
    .pipe(jsFilter)
    .pipe(allPlugins.ngAnnotate())
    .pipe(allPlugins.uglify({ preserveComments: allPlugins.uglifySaveLicense })).on('error', conf.errorHandler('Uglify'))
    .pipe(jsFilter.restore())
    .pipe(cssFilter)
    .pipe(allPlugins.replace('../../bower_components/bootstrap/fonts/', '../fonts/'))
    .pipe(allPlugins.csso())
    .pipe(cssFilter.restore())
    .pipe(assets.restore())
    .pipe(allPlugins.useref())
    .pipe(allPlugins.revReplace())
    .pipe(htmlFilter)
    .pipe(allPlugins.minifyHtml({
      empty: true,
      spare: true,
      quotes: true,
      conditionals: true
    }))
    .pipe(htmlFilter.restore())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')))
    .pipe(allPlugins.size({ title: path.join(conf.paths.dist, '/'), showFiles: true }));
});

// Only applies for fonts from bower dependencies
// Custom fonts are handled by the "other" task
gulp.task('fonts', function () {
  return gulp.src(allPlugins.mainBowerFiles())
    .pipe(allPlugins.filter('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe(allPlugins.flatten())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/fonts/')));
});

gulp.task('other', function () {
  var fileFilter = allPlugins.filter(function (file) {
    return file.stat.isFile();
  });

  return gulp.src([
    path.join(conf.paths.src, '/**/*'),
    path.join('!' + conf.paths.src, '/**/*.{html,css,js,less}')
  ])
    .pipe(fileFilter)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('clean', function (done) {
  allPlugins.del([path.join(conf.paths.dist, '/'), path.join(conf.paths.tmp, '/')], done);
});

gulp.task('build', ['html', 'fonts', 'other']);
