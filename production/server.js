'use strict';

var port = 6030;
var path = require('path');
var http = require('http');
var helmet = require('helmet');
var express = require('express');
var compression = require('compression');
var distPath = path.resolve(__dirname.concat('/dist'));

var appExpress = express();

appExpress.use(helmet());
appExpress.use(compression());
appExpress.use('/', express.static(distPath));

http.Server(appExpress).listen(port);
