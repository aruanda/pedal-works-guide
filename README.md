# Pedal Works Guide
Style Guide of project: Pedal Works

[Demo Version](http://188.226.170.38:6030/)


Environment with:

* [Docker](https://docs.docker.com/)
* [Make](http://www.gnu.org/software/make/manual/make.html#Running)
* [NodeJS](https://nodejs.org/dist/latest-v4.x/docs/api/)


Make tasks of environment

* Build docker image - ```$ make build-image```
* Build docker container - ```$ make build-container```
* Attach docker container - ```$ make attach-container```
* Install all dependencies - ```$ npm install```
* Run Local server - ```$ gulp serve```
